var express = require('express');
const multer = require('multer');
const os = require('os')
const { mustLogin } = require('../utils/must-login');
var router = express.Router();
var booksController = require('./controller');

router.get('/', booksController.index);

router.post('/', mustLogin(), multer({dest: os.tmpdir()}).single('cover'), booksController.store);

module.exports = router;