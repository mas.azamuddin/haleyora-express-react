const path = require('path')
const fs = require('fs')
var models = require('../../models')

function index(req, res){
    return res.json("List books")
}

async function store(req, res){

    const body = req.body;

    if(req.file){
        let tmp_path = req.file.path; 
        console.log(tmp_path, 'tmp_path')

        let originalExt = req.file.originalname.split('.')[req.file.originalname.split('.').length - 1];
        let filename = req.file.filename + '.' + originalExt;

        let target_path = path.resolve(path.resolve(__dirname, '../../'), `public/books/${filename}`)

        const src = fs.createReadStream(tmp_path)
        const dest = fs.createWriteStream(target_path)

        src.pipe(dest)

        src.on('end', async function(){
            
            try {
                let newBook = await models.Book.create({...body, cover_img: filename })
                return res.json(newBook)
            } catch(err) {
        
                if(err.name == "SequelizeValidationError"){
                    return res.json(err)
                }
        
                return res.json(err);
            }
        });
    } else {

        try {
            let newBook = await models.Book.create({...body})
            return res.json(newBook)
        } catch(err) {
    
            if(err.name == "SequelizeValidationError"){
                return res.json(err)
            }
    
            return res.json(err);
        }
    }
    
    
}

module.exports = {
    index,
    store
}