var jwt = require('express-jwt')

const mustLogin = () => jwt({secret: process.env.SECRET_KEY, algorithms: ['HS256']})

module.exports = {
    mustLogin
}