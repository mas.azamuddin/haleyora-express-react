var express = require('express');
var router = express.Router();
var multer = require('multer')


var userController = require('./controller');
const { mustLogin } = require('../utils/must-login');


router.get('/', mustLogin(), userController.index);

router.post('/', multer().none(), userController.store);


router.get("/me", mustLogin(), userController.me); 
router.get('/:userid', userController.findOne);

router.post('/login', multer().none(), userController.login);

router.delete('/:id', mustLogin(), userController.destroy);

router.put('/:id', mustLogin(), multer().none(), userController.update)



module.exports = router;