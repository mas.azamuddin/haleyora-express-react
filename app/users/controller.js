
var { Op } = require('sequelize');
var models = require('../../models')
const bcrypt = require('bcrypt')
const jwtToken = require('jsonwebtoken')
require('dotenv').config();

async function index(req, res){

    const { 
        limit = 10, 
        skip = 0, 
        nama = '', 
        city = '', 
        operator, 
        email = '', 
        sort = ''
    } = req.query;
    
    let users = []
    let count = 0;

    if(operator == 'and'){
         users = await models.User.findAll({
            limit: parseInt(limit), 
            offset: parseInt(skip), 
            where: {
                name: {[Op.like]: "%" + nama + "%"},
                city: {[Op.like]: `%${city}%`}
            }, 
            order: [sort.split(' ')]
        });

        count = await models.User.count({
            limit: parseInt(limit), 
            offset: parseInt(skip), 
            where: {
                name: {[Op.like]: "%" + nama + "%"},
                city: {[Op.like]: `%${city}%`}
            }
        });
    } else {
         users = await models.User.findAll({
            limit: parseInt(limit), 
            offset: parseInt(skip), 
            where: {
                [Op.or]: [
                    {
                        name: {[Op.like]: "%" + nama + "%"}
                    },
                    {
                        city: {[Op.like]: `%${city}%`}
                    }
                ], 
                email: {
                    [Op.like]: `%${email}%`
                }
            },
            order: [sort.split(' ')]
        });
        count = await models.User.count({
            limit: parseInt(limit), 
            offset: parseInt(skip), 
            where: {
                [Op.or]: [
                    {
                        name: {[Op.like]: "%" + nama + "%"}
                    },
                    {
                        city: {[Op.like]: `%${city}%`}
                    }
                ], 
                email: {
                    [Op.like]: `%${email}%`
                }
            }
        });
    }
    
    return res.json({users, count});
}

async function store(req, res){

    const data = req.body;

    // encrypt password
    data.password = bcrypt.hashSync(data.password, 10);

    try {
        const user = await models.User.create(data);
        delete user.password;
        return res.json(user);
    } catch(err) {

        if(err.name == "SequelizeValidationError"){
            return res.json(err)
        }

        return res.json(err);

    }

}

async function findOne(req, res){

    const id = req.params.userid;

    let user = await models.User.findOne({where: {id}});

    return res.json(user)
}

async function login(req, res){
    const { email, password } = req.body; 

    let user = await models.User.findOne({
        where: {
            email
        }
    })

    if(!user){
        return res.json({
            error: 1, 
            message: "User or password invalid"
        });
    }

    // check apakah password valid
    const passwordValid = bcrypt.compareSync(password, user.password);

    // jika tidak valid
    if(!passwordValid){
        return res.json({
            error: 1, 
            message: "User or password invalid"
        });
    }

    // sign atau generate JWT token dengan data user yang minta login
    const token = jwtToken.sign(user.toJSON(), process.env.SECRET_KEY);

    // response dengan token
    return res.json(token);

}

async function me(req, res){

    const user = req.user;

    return res.json(user)
}

async function destroy(req, res){
    const id = req.params.id;

    let destroyed = await models.User.destroy({where: {id}});
    

    return res.json(destroyed);
}

async function update(req, res){
    const id = req.params.id; 
    const data = req.body;

    let user = await models.User.findOne({where: {id}});

    if(user){
        user.update(data);
    }

    return res.json(user);
}

module.exports = {
    index,
    store, 
    findOne, 
    login, 
    me,
    destroy,
    update
}