'use strict';

const isUnique = function(modelName, field, msg) {

  return function(value, next) {
    var Model = require("../models")[modelName];
    var query = {};
    query[field] = value;
    Model.findOne({where: query, attributes: ["id"]}).then(function(obj) {
      if (obj) {
        next(msg.replace('%v', value));
      } else {
        next();
      }
    });
  };
  
}


const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    name: {
      type: DataTypes.STRING, 
      validate: {
        min: 6, 
        max: 10
      }
    },
    city: DataTypes.STRING,
    email: {
      type: DataTypes.STRING, 
      validate: {
        min: 4, 
        max: {
          args: [255], 
          msg: "Panjang karakter tidak boleh melebih 255."
        }, 
        isEmail: {
          args: [true], 
          msg: "Email tidak valid!"
        }, 
        isUnique: isUnique("User", "email", "Email %v sudah terdaftar")
      }
    },
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User'
  });
  return User;
};